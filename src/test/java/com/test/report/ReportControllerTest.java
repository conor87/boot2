package com.test.report;

import com.test.errorhandling.BootExceptionHandler;
import com.test.errorhandling.BootReportNotFoundException;
import com.test.report.model.Report;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.is;

@RunWith(MockitoJUnitRunner.class)
public class ReportControllerTest {

  private MockMvc mockMvc;

  @Mock
  private ReportService reportService;

  @InjectMocks
  private ReportController reportController;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(reportController).setControllerAdvice(new BootExceptionHandler()).build();
  }

  @Test
  public void getReportTest() throws Exception {

    // Given:
    Report expectedReport = new Report("Hello Report");
    ResponseEntity<Report> expectedResponseEntity = new ResponseEntity<Report>(expectedReport, HttpStatus.OK);
    Mockito.when(reportService.getReport()).thenReturn(expectedReport);

    // When:
    mockMvc.perform(MockMvcRequestBuilders.get("/report")
        .accept(MediaType.APPLICATION_JSON))

        // Then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(expectedResponseEntity.getBody().getMessage())));

  }

  @Test
  public void getReportNotFoundTest() throws Exception {

    // Given:
    String expected = ReportServiceImpl.REPORT_NOT_FOUND;
    Mockito.when(reportService.getReport()).thenThrow(new BootReportNotFoundException(expected));

    // When:
    mockMvc.perform(MockMvcRequestBuilders.get("/report")
        .accept(MediaType.APPLICATION_JSON))

        // Then
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(expected)));

  }

}
