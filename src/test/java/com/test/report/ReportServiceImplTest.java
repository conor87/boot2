package com.test.report;

import com.test.errorhandling.BootReportNotFoundException;
import com.test.report.model.Report;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

public class ReportServiceImplTest {

  @Mock
  private RestTemplate restTemplate;

  @InjectMocks
  private ReportServiceImpl reportService;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getReportTest() {

    // Given:
    Report expectedReport = new Report("Hello Report");
    Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any())).thenReturn(expectedReport);

    // When:
    Report actualReport = reportService.getReport();

    // Then:
    Assertions.assertThat(actualReport != null);
    Assertions.assertThat(actualReport.getMessage().equals(expectedReport.getMessage()));
  }

  @Test
  public void getReportNotFoundTest() {

    // Given:
    Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any())).thenReturn(null);

    // When:
    Throwable throwable = Assertions.catchThrowable(() -> reportService.getReport());

    // Then:
    Assertions.assertThat(throwable).isInstanceOf(BootReportNotFoundException.class);

  }

}
