package com.test.errorhandling;

public class BootErrorMessage {

  private final String message;
  private final String description;

  public BootErrorMessage(String message, String description) {
    this.message = message;
    this.description = description;
  }

  public String getMessage() {
    return message;
  }

  public String getDescription() {
    return description;
  }
}
