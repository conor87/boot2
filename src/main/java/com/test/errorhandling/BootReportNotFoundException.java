package com.test.errorhandling;

public class BootReportNotFoundException extends RuntimeException {

  public BootReportNotFoundException(String message){
    super(message);
  }

}
