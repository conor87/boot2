package com.test.errorhandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.ConnectException;

@ControllerAdvice
public class BootExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(BootExceptionHandler.class);
  public static final String COULD_NOT_CONNECT_TO_EXTERNAL_SERVICE = "Could not connect to external service";
  public static final String NOT_FOUND = "Not Found";

  @ExceptionHandler(ConnectException.class)
  public final ResponseEntity<BootErrorMessage> connectionException(ConnectException connectionException) {

    LOGGER.error("ERROR", connectionException);

    BootErrorMessage bootErrorMessage = new BootErrorMessage(connectionException.getMessage(), COULD_NOT_CONNECT_TO_EXTERNAL_SERVICE);
    return new ResponseEntity<BootErrorMessage>(bootErrorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(BootReportNotFoundException.class)
  public final ResponseEntity<BootErrorMessage> bootReportNotFoundException(BootReportNotFoundException bootReportNotFoundException) {

    LOGGER.error("ERROR", bootReportNotFoundException);

    BootErrorMessage bootErrorMessage = new BootErrorMessage(bootReportNotFoundException.getMessage(), NOT_FOUND);
    return new ResponseEntity<BootErrorMessage>(bootErrorMessage, new HttpHeaders(), HttpStatus.NOT_FOUND);
  }
}