package com.test.report;

import com.test.errorhandling.BootReportNotFoundException;
import com.test.report.model.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ReportServiceImpl implements ReportService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);

  public static final String REPORT_NOT_FOUND = "Report not found";
  public static final String GREETING = "/greeting";

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Value("${boot.baseurl}")
  private String baseUrl;

  @Override
  public Report getReport() {

    Report report = restTemplate.getForObject(baseUrl + GREETING, Report.class);

    if(report == null){
      throw new BootReportNotFoundException(REPORT_NOT_FOUND);
    }

    return report;
  }

  @Override
  public void sendReportMessage(Report report){
    LOGGER.info("Sending message to report-queue: {}", report.getMessage());
    rabbitTemplate.convertAndSend("report-queue", report.getMessage());
  }
}
