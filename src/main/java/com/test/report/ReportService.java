package com.test.report;

import com.test.report.model.Report;
import org.springframework.stereotype.Service;

/**
 * Created by conororourke on 15/11/2017.
 */
@Service
public interface ReportService {
  Report getReport();
  void sendReportMessage(Report report);
}
