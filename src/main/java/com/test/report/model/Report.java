package com.test.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Report {

  private String message;

  public Report(String message) {
    this.message = message;
  }

  public Report() {
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "Report{" +
        "message='" + message + '\'' +
        '}';
  }
}
