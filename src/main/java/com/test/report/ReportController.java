package com.test.report;

import com.test.report.model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReportController {

  private ReportService reportService;

  @Autowired
  public ReportController(ReportService reportService) {
    this.reportService = reportService;
  }

  @RequestMapping(value = "/report", method = RequestMethod.GET)
  public ResponseEntity<Report> getReport() {
    Report report = reportService.getReport();
    return new ResponseEntity<>(report, HttpStatus.OK);
  }

  @RequestMapping(value = "/report", method = RequestMethod.POST)
  public ResponseEntity<Report> sendReport(@RequestBody Report report) {
    reportService.sendReportMessage(report);
    return new ResponseEntity<>(report, HttpStatus.ACCEPTED);
  }

}
